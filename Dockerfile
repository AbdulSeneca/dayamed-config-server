FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} app.jar

EXPOSE 8888 

ENTRYPOINT ["java","-Dspring.config.location=config-repo -Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]