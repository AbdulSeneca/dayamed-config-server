package com.sg.dayamed.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class DayamedConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DayamedConfigServerApplication.class, args);
	}
}
